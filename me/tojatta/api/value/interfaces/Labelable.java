package me.tojatta.api.value.interfaces;

/**
 * Created by Tojatta on 7/12/2016.
 */
public interface Labelable {

    String getLabel();

}
