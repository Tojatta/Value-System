package me.tojatta.api.value.impl;

import me.tojatta.api.value.Value;
import me.tojatta.api.value.interfaces.Toggleable;

import java.lang.reflect.Field;

/**
 * Created by Tojatta on 7/12/2016.
 */
public class TypeBoolean extends Value<Boolean> implements Toggleable {

    public TypeBoolean(String label, Object object, Field field) {
        super(label, object, field);
    }

    @Override
    public boolean isEnabled() {
        return this.getValue();
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.setValue(enabled);
        if (enabled) {
            onEnable();
        } else {
            onDisable();
        }
        onToggle();
    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }

    @Override
    public void onToggle() {

    }

    @Override
    public void toggle() {
        setEnabled(!isEnabled());
    }
}
