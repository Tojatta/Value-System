package me.tojatta.api.value.impl;

import me.tojatta.api.value.Value;

import java.lang.reflect.Field;

/**
 * Created by Tojatta on 7/12/2016.
 */
public class TypeNumber<T extends Number> extends Value<T> {

    private T minimum;

    private T maximum;

    public TypeNumber(String label, Object object, Field field, T minimum, T maximum) {
        super(label, object, field);
        this.minimum = minimum;
        this.maximum = maximum;
    }

    @Override
    public void setValue(T value) {
        if (value.doubleValue() > maximum.doubleValue()) {
            value = maximum;
        } else if (value.doubleValue() < minimum.doubleValue()) {
            value = minimum;
        }
        super.setValue(value);
    }

    public T getMinimum() {
        return minimum;
    }

    public void setMinimum(T minimum) {
        this.minimum = minimum;
    }

    public T getMaximum() {
        return maximum;
    }

    public void setMaximum(T maximum) {
        this.maximum = maximum;
    }

}
