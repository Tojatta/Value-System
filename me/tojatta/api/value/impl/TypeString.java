package me.tojatta.api.value.impl;

import me.tojatta.api.value.Value;

import java.lang.reflect.Field;

/**
 * Created by Tojatta on 7/12/2016.
 */
public class TypeString extends Value<String> {

    public TypeString(String label, Object object, Field field) {
        super(label, object, field);
    }

}
