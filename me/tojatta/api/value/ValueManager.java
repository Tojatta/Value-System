package me.tojatta.api.value;

import me.tojatta.api.value.impl.TypeBoolean;
import me.tojatta.api.value.impl.TypeNumber;
import me.tojatta.api.value.impl.TypeString;
import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.tojatta.api.value.impl.annotations.NumberValue;
import me.tojatta.api.value.impl.annotations.StringValue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * Created by Tojatta on 7/12/2016.
 */
public class ValueManager {

    public static final String AUTHOR = "Tojatta";

    private HashMap<Value, Class> values;

    public ValueManager() {
        values = new HashMap<>();
    }

    public HashMap<Value, Class> getValues() {
        return values;
    }

    public Optional<Value> getOptionalValueName(String name) {
        return values.keySet().stream().filter(value -> name.equalsIgnoreCase(value.getLabel())).findAny();
    }

    public List<Value> getValuesFromClass(Object object) {
        List<Value> valueList = new ArrayList<>();
        values.entrySet().stream().filter(valueClassEntry -> valueClassEntry.getValue().equals(object.getClass())).forEach(valueClassEntry -> valueList.add(valueClassEntry.getKey()));
        return valueList;
    }

    public final void register(Object o) {
        if (o == null) {
            return;
        }
        Class<?> objectClass = o.getClass();
        for (Field field : objectClass.getDeclaredFields()) {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            if (field.isAnnotationPresent(BooleanValue.class)) {
                BooleanValue annotation = field.getAnnotation(BooleanValue.class);
                TypeBoolean booleanValue = new TypeBoolean(annotation.label(), o, field);
                values.put(booleanValue, objectClass);
                System.out.println(booleanValue.getLabel());
            } else if (field.isAnnotationPresent(StringValue.class)) {
                StringValue annotation = field.getAnnotation(StringValue.class);
                TypeString stringValue = new TypeString(annotation.label(), o, field);
                values.put(stringValue, objectClass);
                System.out.println(stringValue.getLabel());
            } else if (field.isAnnotationPresent(NumberValue.class)) {
                if(!field.getType().isPrimitive()){
                    continue;
                }
                NumberValue annotation = field.getAnnotation(NumberValue.class);
                TypeNumber numberValue = null;
                if (field.getType().isAssignableFrom(Byte.TYPE)) {
                    numberValue = new TypeNumber(annotation.label(), o, field, Byte.parseByte(annotation.minimum()), Byte.parseByte(annotation.maximum()));
                } else if (field.getType().isAssignableFrom(Short.TYPE)) {
                    numberValue = new TypeNumber(annotation.label(), o, field, Short.parseShort(annotation.minimum()), Short.parseShort(annotation.maximum()));
                } else if (field.getType().isAssignableFrom(Integer.TYPE)) {
                    numberValue = new TypeNumber(annotation.label(), o, field, Integer.parseInt(annotation.minimum()), Integer.parseInt(annotation.maximum()));
                } else if (field.getType().isAssignableFrom(Long.TYPE)) {
                    numberValue = new TypeNumber(annotation.label(), o, field, Long.parseLong(annotation.minimum()), Long.parseLong(annotation.maximum()));
                } else if (field.getType().isAssignableFrom(Double.TYPE)) {
                    numberValue = new TypeNumber(annotation.label(), o, field, Double.parseDouble(annotation.minimum()), Double.parseDouble(annotation.maximum()));
                } else if (field.getType().isAssignableFrom(Float.TYPE)) {
                    numberValue = new TypeNumber(annotation.label(), o, field, Float.parseFloat(annotation.minimum()), Float.parseFloat(annotation.maximum()));
                }
                if (numberValue != null) {
                    System.out.println(numberValue.getLabel());
                    values.put(numberValue, objectClass);
                }
            }
            field.setAccessible(accessible);
        }

    }

}
